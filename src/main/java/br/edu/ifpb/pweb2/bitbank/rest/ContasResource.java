package br.edu.ifpb.pweb2.bitbank.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifpb.pweb2.bitbank.model.Conta;
import br.edu.ifpb.pweb2.bitbank.repository.ContaRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API Rest de Contas")
public class ContasResource {

	@Autowired
	private ContaRepository contaRepo;

	@GetMapping(value = "/contas", produces = "application/json")
	@ApiOperation(value = "Retorna todas as contas cadastradas.")
	public List<Conta> findAllContas() {
		return contaRepo.findAll();
	}

	@GetMapping(value = "/contas/{id}", produces = "application/json")
	@ApiOperation(value = "Retorna a conta cujo id é informado.")
	public Conta findContaPorId(@PathVariable("id") Integer id) {
		return contaRepo.getById(id);
	}

	@PostMapping(value = "/contas", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Insere uma nova conta.")
	private Conta adicioneCorrentista(@RequestBody Conta conta) {
		return contaRepo.save(conta);
	}

}
