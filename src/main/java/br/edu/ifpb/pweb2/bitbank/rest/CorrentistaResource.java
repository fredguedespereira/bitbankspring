package br.edu.ifpb.pweb2.bitbank.rest;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;
import br.edu.ifpb.pweb2.bitbank.util.PasswordUtil;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
public class CorrentistaResource {
	
	@Autowired
	private CorrentistaRepository correntistaRepo;
	
	@GetMapping(value = "/correntistas", produces = "application/json")
	@ApiOperation(value="Retorna todos os correntistas cadastrados.")
	public List<Correntista> findAllCorrentistas() {
		return correntistaRepo.findAll();
	}
	
	@GetMapping(value = "/correntistas/{id}" ,produces = "application/json")
	@ApiOperation(value="Retorna todos o correntista cujo id é informado.")
	public Correntista findCorrentistaPorId(@PathVariable("id") Integer id) {
		return correntistaRepo.getById(id);
	}
	 
	@GetMapping(value="/correntistas/admin", produces = "application/json")
	@ApiOperation(value="Retorna todos os correntistas que são administradores.")
	private List<Correntista> getCorrentistasAdmin() {
		return correntistaRepo.findAll().stream().filter(e -> e.isAdmin()).collect(Collectors.toList());
	}
	
	@PostMapping(value="/correntistas/", produces = "application/json", consumes = "application/json")
	@ApiOperation(value="Grava um novo correntista na base.")
	private Correntista adicioneCorrentista(@RequestBody Correntista correntista) {
		correntista.setSenha(PasswordUtil.hashPassword(correntista.getSenha()));
		return correntistaRepo.save(correntista);
	}
	
	@PutMapping(value="/correntistas/", produces = "application/json", consumes = "application/json")
	@ApiOperation(value="Atualiza um novo correntista na base.")
	private Correntista atualizeCorrentista(@RequestBody Correntista correntista) {
		correntista.setSenha(PasswordUtil.hashPassword(correntista.getSenha()));
		return correntistaRepo.save(correntista);
	}
	
	@DeleteMapping(value="/correntistas", produces = "application/json", consumes = "application/json")
	@ApiOperation(value="Exclui um correntista da base.")
	private void excluaCorrentista(@RequestBody Correntista correntista) {
		correntistaRepo.delete(correntista);
	}
	
	@PostMapping(value = "/correntistas/{id}/delete" ,produces = "application/json")
	@ApiOperation(value="Exclui um correntista da base dado o id dele")
	public void excluiCorrentistaPorId(@PathVariable("id") Integer id) {
		Optional<Correntista> correntista = correntistaRepo.findById(id);
		if (correntista.isPresent()) {
			correntistaRepo.delete(correntista.get());
		}
	}

}
