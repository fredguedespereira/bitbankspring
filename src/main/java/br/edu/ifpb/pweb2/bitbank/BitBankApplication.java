package br.edu.ifpb.pweb2.bitbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import br.edu.ifpb.pweb2.bitbank.filter.LoginFilter;

@SpringBootApplication
public class BitBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitBankApplication.class, args);
	}

	@Bean
	public FilterRegistrationBean<LoginFilter> filterRegistrationBean() {
		FilterRegistrationBean<LoginFilter> registrationBean = new FilterRegistrationBean<LoginFilter>();
		LoginFilter customURLFilter = new LoginFilter();

		registrationBean.setFilter(customURLFilter);
		// define quais URLs o filtro interceptará (se não informar, intercepta todas e,
		// daí, fica em loop infinito)
		registrationBean.addUrlPatterns("/contas/*");
		registrationBean.addUrlPatterns("/correntistas/*");
		// define a precedência do filtro (primeiro, caso aparece outro depois)
		registrationBean.setOrder(1);
		return registrationBean;
	}

}
