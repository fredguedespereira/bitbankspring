package br.edu.ifpb.pweb2.bitbank.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.pweb2.bitbank.model.ContaForm;
import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.repository.ContaRepository;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;

@Controller
@RequestMapping("/contas")
public class ContaController {
	
	@RequestMapping("/form")
	public ModelAndView getForm(ModelAndView modelAndView, ContaForm contaForm) {
		modelAndView.setViewName("contas/form");
		modelAndView.addObject("contaForm", contaForm);
		return modelAndView;
	}
	
	@ModelAttribute (value="correntistaItems")
	public List<Correntista> getCorrentistas() {
		return CorrentistaRepository.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView adicioneConta(ContaForm contaForm, ModelAndView modelAndView, RedirectAttributes attr) {
		Correntista correntista 
			= CorrentistaRepository.findById(contaForm.getCorrentistaId());
		// Pega um novo ID para insert de novas contas ou o id da conta corrente para update
		Integer novoId = (contaForm.getConta().getId() == null) ? ContaRepository.getMaxId() : contaForm.getConta().getId();  
		contaForm.getConta().setId(novoId);
		contaForm.getConta().setCorrentista(correntista);
		ContaRepository.store(contaForm.getConta());
		attr.addFlashAttribute("mensagem", "Conta cadastrada com sucesso!");
		modelAndView.setViewName("redirect:/contas");
		return modelAndView;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView liste(ModelAndView modelAndView) {
		modelAndView.setViewName("contas/list");
		modelAndView.addObject("contas", ContaRepository.findAll());
		return modelAndView;
	}

	@RequestMapping("/{id}")
	public ModelAndView getContaById(@PathVariable(value = "id") Integer id, 
			ModelAndView modelAndView, ContaForm contaForm) {
		contaForm.setConta(ContaRepository.findById(id));
		modelAndView.addObject("contaForm", contaForm);
		modelAndView.setViewName("contas/form");
		return modelAndView;
	}
	
	@RequestMapping("/{id}/delete")
	public ModelAndView deleteById(@PathVariable(value = "id") Integer id, 
			ModelAndView mav, RedirectAttributes attr) {
		ContaRepository.deleteById(id);
		attr.addFlashAttribute("mensagem", "Conta removida com sucesso!");
		mav.setViewName("redirect:/contas");
		return mav;
	}


}
