package br.edu.ifpb.pweb2.bitbank.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.pweb2.bitbank.converter.BigDecimalEditor;
import br.edu.ifpb.pweb2.bitbank.model.Conta;
import br.edu.ifpb.pweb2.bitbank.model.ContaForm;
import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.model.Transacao;
import br.edu.ifpb.pweb2.bitbank.repository.ContaRepository;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;

@Controller
@RequestMapping("/contas")
public class ContaController {
	
	@Autowired
	private ContaRepository contaRepository;
	
	@Autowired
	private CorrentistaRepository correntistaRepository;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(BigDecimal.class, new BigDecimalEditor());
    }
	
	@RequestMapping("/form")
	public ModelAndView getForm(ModelAndView modelAndView, Conta conta, Integer idCorrentista) {
		modelAndView.setViewName("contas/form");
		modelAndView.addObject("conta", conta);
		modelAndView.addObject("idCorrentista", idCorrentista);
		return modelAndView;
	}
	
	@ModelAttribute (value="correntistaItems")
	public List<Correntista> getCorrentistas() {
		return correntistaRepository.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView adicioneConta(@Valid Conta conta, BindingResult result, Integer idCorrentista, ModelAndView modelAndView, RedirectAttributes attr) {
		if (result.hasErrors()) {
			modelAndView.addObject("idCorrentista", idCorrentista);
			modelAndView.setViewName("/contas/form");
			return modelAndView;
		}
		Correntista correntista 
			= correntistaRepository.getById(idCorrentista);
		// Pega um novo ID para insert de novas contas ou o id da conta corrente para update
		conta.setCorrentista(correntista);
		contaRepository.save(conta);
		attr.addFlashAttribute("mensagem", "Conta cadastrada com sucesso!");
		modelAndView.setViewName("redirect:/contas");
		return modelAndView;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView liste(ModelAndView modelAndView) {
		modelAndView.setViewName("contas/list");
		modelAndView.addObject("contas", contaRepository.findAll());
		return modelAndView;
	}

	@RequestMapping("/{id}")
	public ModelAndView getContaById(@PathVariable(value = "id") Integer id, 
			ModelAndView modelAndView, ContaForm contaForm) {
		contaForm.setConta(contaRepository.getById(id));
		modelAndView.addObject("contaForm", contaForm);
		modelAndView.setViewName("contas/form");
		return modelAndView;
	}
	
	@RequestMapping("/{id}/delete")
	public ModelAndView deleteById(@PathVariable(value = "id") Integer id, 
			ModelAndView mav, RedirectAttributes attr) {
		contaRepository.deleteById(id);
		attr.addFlashAttribute("mensagem", "Conta removida com sucesso!");
		mav.setViewName("redirect:/contas");
		return mav;
	}
	
	@RequestMapping(value="/operacao")
	public ModelAndView operacaoConta(String nuConta, Transacao transacao, 
			ModelAndView model, RedirectAttributes attr) {
		String proxPagina = "";
		if (nuConta == null) {
			model.addObject("transacao", new Transacao());
			proxPagina = "contas/operacao";
		} else {
			if (nuConta != null && transacao.getValor() == null) {
				Conta conta = contaRepository.findByNumeroWithTransacoes(nuConta);
				if (conta != null) {
					model.addObject("conta", conta);
					proxPagina = "contas/operacao";
				} else {
					model.addObject("mensagem", "Conta inexistente!");
					proxPagina = "contas/operacao";
				}
			} else {
				Conta conta = contaRepository.findByNumeroWithTransacoes(nuConta);
				conta.addTransacao(transacao);
				contaRepository.save(conta);
				proxPagina = "redirect:/contas/"+conta.getId()+"/transacoes";
			}
		}
		model.setViewName(proxPagina);
		return model;
	}
	
	@RequestMapping(value="/{id}/transacoes")
	public String addTransacaoConta(@PathVariable("id") Integer idConta, Model model) {
//		Conta conta = contaRepository.findByIdWithTransacoes(idConta);
		Conta conta = contaRepository.busqueContaComTransacoesQueryJPQL(idConta);
		model.addAttribute("conta", conta);
		return "contas/transacoes";
	}

}
