package br.edu.ifpb.pweb2.bitbank.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tb_conta")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Conta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "nu_id")
	private Integer id;

	@Column(name = "nu_numero")
	@NotBlank(message = "Campo obrigatório!")
	@Digits(integer = 6, fraction = 0, message = "Informe um número de até 6 dígitos!")
	private String numero;

	@Column(name = "dt_criacao")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Future(message = "Data deve ser futura!")
	private Date data;

	@OneToMany(mappedBy = "conta", cascade = { CascadeType.ALL })
	private Set<Transacao> transacoes = new HashSet<Transacao>();

	@OneToOne
	@JoinColumn(name = "id_correntista", foreignKey = @ForeignKey(name = "FK_CORRENTISTA"))
	@JsonBackReference
	private Correntista correntista;

	public BigDecimal getSaldo() {
		BigDecimal total = BigDecimal.ZERO;
		for (Transacao t : this.transacoes) {
			total = total.add(t.getValor());
		}
		return total;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Set<Transacao> getTransacoes() {
		return transacoes;
	}

	public void setTransacoes(Set<Transacao> transacoes) {
		this.transacoes = transacoes;
	}

	public void addTransacao(Transacao transacao) {
		this.transacoes.add(transacao);
		transacao.setConta(this);

	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Correntista getCorrentista() {
		return correntista;
	}

	public void setCorrentista(Correntista correntista) {
		this.correntista = correntista;
	}

	@Override
	public String toString() {
		return "Conta [numero=" + numero + ", data=" + data + "]";
	}

}
