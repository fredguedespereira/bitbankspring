package br.edu.ifpb.pweb2.bitbank.model;

import javax.validation.constraints.NotNull;

public class ContaForm {
	
	@NotNull(message="A conta não pode ser nula!")
	private Conta conta;
	
	@NotNull(message="Selecione um correntista!")
	private Integer correntistaId;

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Integer getCorrentistaId() {
		return correntistaId;
	}

	public void setCorrentistaId(Integer correntistaId) {
		this.correntistaId = correntistaId;
	}
}

