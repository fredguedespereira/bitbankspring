package br.edu.ifpb.pweb2.bitbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;
import br.edu.ifpb.pweb2.bitbank.util.PasswordUtil;

@Controller
@RequestMapping("/populate")
public class DataController {
	
	@Autowired
	private CorrentistaRepository correntistaRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView populateCorrentistas(ModelAndView mav) {
		correntistaRepository.save(
				new Correntista(1, "Charles Darwin", "darwin@imperialcollege.edu.uk", PasswordUtil.hashPassword("12345"), false));
		correntistaRepository.save(
				new Correntista(2, "Carl Sagan", "sagan@nasa.gov", PasswordUtil.hashPassword("12345"), false));
		correntistaRepository.save(
				new Correntista(3, "Richard Dawkins", "dawkins@dawkinsfoundations.org", PasswordUtil.hashPassword("12345"), false));
		correntistaRepository.save(
				new Correntista(3, "Administrador", "admin@bitbank.com", PasswordUtil.hashPassword("admin"), true));
		mav.setViewName("redirect:/login");
		return mav;
	}

}
