package br.edu.ifpb.pweb2.bitbank.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;

@Controller
@RequestMapping("/populate")
public class DataController {
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView populateCorrentistas(ModelAndView mav) {
		CorrentistaRepository.store(
				new Correntista(1, "Charles Darwin", "darwin@imperialcollege.edu.uk", "12345"));
		CorrentistaRepository.store(
				new Correntista(2, "Carl Sagan", "sagan@nasa.gov", "12345"));
		CorrentistaRepository.store(
				new Correntista(3, "Richard Dawkins", "dawkins@dawkinsfoundations.org", "12345"));
		mav.setViewName("redirect:/correntistas");
		return mav;
	}

}
