package br.edu.ifpb.pweb2.bitbank.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="tb_correntista")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Correntista implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "nu_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nm_nome")
	@NotBlank(message="Campo obrigatório")
	private String nome;
	
	@Column(name = "nm_email")
	@NotBlank(message="Campo obrigatório")
	private String email;
	
	@Column(name = "nm_senha")
	@NotBlank(message="Campo obrigatório")
	@Size(min=3, message = "Senha deve ter no mínimo 3 caracteres")
	private String senha;
	
	@Column (name = "tp_user")
	private boolean admin;
	
	public Correntista() {
		// Para a classe ser um JavaBean
	}

	public Correntista(Integer id, String nome, String email, String senha, boolean admin) {
		super();
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.senha = senha;
		this.admin = admin;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public String toString() {
		return nome;
	}
	
	
}
