package br.edu.ifpb.pweb2.bitbank.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.repository.ContaRepository;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;

@Controller
@RequestMapping("/correntistas")
public class CorrentistaController {
	
	@RequestMapping("/form")
	public String getForm(Correntista correntista, Model model) {
		model.addAttribute("correntista", correntista);
		return "correntistas/form";
	}

	@RequestMapping(value="/save", method = RequestMethod.POST)
	public String save(Correntista correntista, RedirectAttributes redirectAttributes) {
		correntista.setId(CorrentistaRepository.getMaxId());
		CorrentistaRepository.store(correntista);
		redirectAttributes.addFlashAttribute("mensagem", "Correntista cadastrado com sucesso!");
		return "redirect:/correntistas";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listAll(ModelAndView modelAndView) {
		modelAndView.addObject("correntistas", CorrentistaRepository.findAll());
		modelAndView.setViewName("correntistas/list");
		return modelAndView;
	}
	
	@RequestMapping("/{id}")
	public String getCorrentistaById(@PathVariable(value = "id") Integer id, Model model) {
		model.addAttribute("correntista", CorrentistaRepository.findById(id));
		return "correntistas/form";
	}
	
	@RequestMapping("/{id}/delete")
	public ModelAndView deleteById(@PathVariable(value = "id") Integer id, 
			ModelAndView mav, RedirectAttributes attr) {
		CorrentistaRepository.deleteById(id);
		attr.addFlashAttribute("mensagem", "Correntista e conta removidos com sucesso!");
		mav.setViewName("redirect:/correntistas");
		return mav;
	}
}
