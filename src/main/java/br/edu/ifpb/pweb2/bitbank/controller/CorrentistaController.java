package br.edu.ifpb.pweb2.bitbank.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.pweb2.bitbank.model.Correntista;
import br.edu.ifpb.pweb2.bitbank.repository.CorrentistaRepository;
import br.edu.ifpb.pweb2.bitbank.util.PasswordUtil;

@Controller
@RequestMapping("/correntistas")
public class CorrentistaController {
	
	@Autowired
	private CorrentistaRepository correntistaRepository;
	
	@RequestMapping("/form")
	public String getForm(Correntista correntista, Model model) {
		model.addAttribute("correntista", correntista);
		return "correntistas/form";
	}

	@RequestMapping(value="/save", method = RequestMethod.POST)
	public String save(@Valid Correntista correntista, 
			BindingResult result, 
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
            return "correntistas/form";
        }
		correntista.setSenha(PasswordUtil.hashPassword(correntista.getSenha()));
		correntistaRepository.save(correntista);
		redirectAttributes.addFlashAttribute("mensagem", 
				"Correntista cadastrado com sucesso!");
		return "redirect:/correntistas";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listAll(ModelAndView modelAndView) {
		modelAndView.addObject("correntistas", correntistaRepository.findAll());
		modelAndView.setViewName("correntistas/list");
		return modelAndView;
	}
	
	@RequestMapping("/{id}")
	public String getCorrentistaById(@PathVariable(value = "id") Integer id, Model model) {
		model.addAttribute("correntista", correntistaRepository.findById(id));
		return "correntistas/form";
	}
	
	@RequestMapping("/{id}/delete")
	public ModelAndView deleteById(@PathVariable(value = "id") Integer id, 
			ModelAndView mav, RedirectAttributes attr) {
		correntistaRepository.deleteById(id);
		attr.addFlashAttribute("mensagem", "Correntista e conta removidos com sucesso!");
		mav.setViewName("redirect:/correntistas");
		return mav;
	}
}

