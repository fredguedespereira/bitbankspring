package br.edu.ifpb.pweb2.bitbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.ifpb.pweb2.bitbank.model.Correntista;

public interface CorrentistaRepository extends JpaRepository<Correntista, Integer> {
	
	Correntista findByEmail(String email);
	
}

