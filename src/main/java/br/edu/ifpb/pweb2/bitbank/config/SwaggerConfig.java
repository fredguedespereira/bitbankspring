package br.edu.ifpb.pweb2.bitbank.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

	private ApiInfo apiInfo() {
		return new ApiInfo("Bitbank Rest API", "API para acesso a recursos do BitBank via REST", "1.0", "",
				new Contact("IFPB", "www.ifpb.edu.br", "fred@ifpb.edu.br"), "", "", Collections.emptyList());
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.edu.ifpb.pweb2.bitbank.rest")).build()
				.useDefaultResponseMessages(false).apiInfo(apiInfo());
	}

}
